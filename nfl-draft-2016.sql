/*
First round of the NFL 2016 Draft
Pulled together by Dan Huynh (http://DanHuynh.co)
*/

CREATE TABLE nfl_draft(
	id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
	round INTEGER,
	pick_number INTEGER,
	overall_pick INTEGER,
	first_name TEXT,
	last_name TEXT,
	position TEXT,
	height INTEGER,
	weight INTEGER,
	college TEXT,
	team TEXT);

INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,1,1,"Jared","Goff","QB",76,215,"California","Rams");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,2,2,"Carson","Wentz","QB",77,237,"NDSU","Eagles");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,3,3,"Joey","Bosa","DE",77,269,"Ohio State","Chargers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,4,4,"Ezekiel","Elliott","RB",72,225,"Ohio State","Dallas");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,5,5,"Jalen","Ramsey","CB",73,209,"Florida St","Jaguars");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,6,6,"Ronnie","Stanley","OT",78,312,"Notre Dame","Ravens");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,7,7,"DeForest","Buckner","DE",79,291,"Oregon","49ers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,8,8,"Jack","Conklin","OT",78,308,"Mich St","Titans");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,9,9,"Leonard","Floyd","OLB",78,244,"Georgia","Chicago");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,10,10,"Eli","Apple","CB",72,199,"Ohio State","Giants");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,11,11,"Vernon","Hargreaves III","CB",71,204,"Florida","Buccaneers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,12,12,"Sheldon","Rankins","DT",73,299,"Louisville","Saints");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,13,13,"Laremy","Tunsil","OT",77,310,"Ole Miss","Dolphins");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,14,14,"Karl","Joseph","S",70,204,"West Virginia","Raiders");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,15,15,"Corey","Coleman","WR",71,194,"Baylor","Browns");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,16,16,"Taylor","Decker","OT",79,310,"Ohio State","Lions");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,17,17,"Keanu","Neal","S",72,211,"Florida","Atlanta");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,18,18,"Ryan","Kelly","C",76,311,"Alabama","Colts");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,19,19,"Shaq","Lawson","DE",75,269,"Clemson","Bills");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,20,20,"Darron","Lee","OLB",72,232,"Ohio State","Jets");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,21,21,"Will","Fuller","WR",72,186,"Notre Dame","Texans");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,22,22,"Josh","Doctson","WR",74,202,"TCU","Redskins");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,23,23,"Laquon","Treadwell","WR",74,221,"Ole Miss","Vikings");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,24,24,"William","Jackson III","CB",72,189,"Houston","Bengals");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,25,25,"Artie","Burns","CB",72,93,"Miami","Steelers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,26,26,"Paxton","Lynch","QB",78,244,"Memphis","Denver");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,27,27,"Kenny","Clark","DT",74,314,"UCLA","Packers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,28,28,"Joshua","Garnett","WR",76,312,"Stanford","49ers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,29,29,"Robert","Nkemdiche","DT",75,294,"Ole Miss","Cardinals");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,30,30,"Vernon","Bulter","DT",76,323,"Louisiana Tech","Panthers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (1,31,31,"Germain","Ifedi","OT",78,324,"Texas A&M","Seahawks");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,1,32,"Emmanuel","Ogbah","DE",76,273,"Oklahoma St","Browns");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,2,33,"Kevin","Dodd","DE",77,277,"Clemson","Titans");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,3,34,"Jaylon","Smith","OLB",74,223,"Notre Dame","Cowboys");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,4,35,"Hunter","Henry","TE",77,250,"Arkansas","Chargers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,5,36,"Myles","Jack","OLB",73,245,"UCLA","Jaguars");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,6,37,"Chris","Jones","DT",78,310,"Mississippi St","Chiefs");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,7,38,"Xavien","Howard","CB",72,201,"Baylor","Dolphins");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,8,39,"Noah","Spence","DE",74,251,"Eastern Kentucky","Bucccaneers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,9,40,"Sterling","Shepard","WR",70,194,"Oklahoma","Giants");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,10,41,"Reggie","Ragland","ILB",73,247,"Alabama","Bills");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,11,42,"Kamalel","Correa","DE",75,243,"Boise St","Ravens");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,12,43,"Austin","Johnson","OT",76,314,"Penn St","Titans");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,13,44,"Jihad","Ward","DE",77,297,"Illinois","Raiders");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,14,45,"Derrick","Henry","RB",75,247,"Alabama","Titans");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,15,46,"A'Shawn","Robinson","DT",76,307,"Alabama","Lions");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,16,47,"Michael","Thomas","WR",75,212,"Ohio State","Saints");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,17,48,"Jason","Spriggs","OT",75,301,"Indiana","Packers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,18,49,"Jarran","Reed","DT",75,307,"Alabama","Seahawks");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,19,50,"Nick","Martin","OT",76,299,"Notre Dame","Texans");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,20,51,"Christian","Hackenberg","QB",76,223,"Penn St","Jets");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,21,52,"Deion","Jones","OLB",73,222,"LSU","Falcons");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,22,53,"Su'a","Cravens","OLB",73,226,"USC","Redskins");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,23,54,"Mackensie","Alexander","CB",70,190,"Clemson","Vikings");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,24,55,"Tyler","Boyd","WR",73,197,"Pittsburgh","Bengals");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,25,56,"Cody","Whitehair","OT",76,301,"Kansas St","Bears");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,26,57,"T.J.","Green","S",74,209,"Clemson","Colts");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,27,58,"Sean","Davis","CB",73,201,"Maryland","Steelers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,28,59,"Roberto","Aguayo","K",72,207,"Florida St","Buccaneers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,29,60,"Cyrus","Jones","CB",70,197,"Alabama","Patriots");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,30,61,"Vonn","Bell","S",71,199,"Ohio State","Saints");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,30,62,"James","Bradberry","CB",72,211,"Samford","Pathers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (2,30,63,"Adam","Gotsis","DT",76,287,"Georgia Tech","Broncos");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,1,64,"Kevin","Byard","S",71,216,"Middle Tennessee St","Titans");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,2,65,"Carl","Nassib","DE",79,277,"Penn St","Browns");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,3,66,"Max","Tuerk","OL",77,298,"USC","Chargers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,4,67,"Maliek","Collins","DT",74,311,"Nebraska","Cowboys");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,5,68,"Will","Redmond","CB",71,182,"Mississippi St","49ers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,6,69,"Yannick","Ngakoue","DE",74,252,"Maryland","Jaguars");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,7,70,"Bronson","Kaufusi","DE",78,285,"BYU","Ravens");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,8,71,"Darian","Thompson","S",74,208,"Boise St","Giants");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,9,72,"Johnathan","Bullard","DT",75,285,"Florida","Bears");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,10,73,"Kenyan","Drake","RB",73,210,"Alabama","Dolphins");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,11,74,"KeiVarae","Russell","CB",71,192,"Notre Dame","Chiefs");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,12,75,"Shilique","Calhoun","DE",76,251,"Michigan St","Raiders");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,13,76,"Shon","Coleman","OL",77,307,"Auburn","Browns");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,14,77,"Daryl","Worley","CB",73,204,"West Virginia","Panthers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,15,78,"Joe","Thuney","OL",77,304,"N.C. State","Patriots");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,16,79,"Isaac","Seumalo","OL",76,303,"Oregon St","Eagles");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,17,80,"Adolphus","Washington","DT",75,301,"Ohio St","Bills");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,18,81,"Austin","Hooper","TE",76,254,"Stanford","Falcons");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,19,82,"Le'Raven","Clark","OT",77,316,"Texas Tech","Colts");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,20,83,"Jordan","Jenkins","OLB",75,259,"Georgia","Jets");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,21,84,"Kendall","Fuller","CB",71,187,"Virginia Tech","Redskins");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,22,85,"Braxton","Miller","WR",73,201,"Ohio St","Texans");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,23,86,"Leonte","Carroo","WR",72,211,"Rutgers","Dolphins");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,24,87,"Nick","Vigil","OLB",74,245,"Utah St","Bengals");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,25,88,"Kyler","Frackrell","OLB",77,245,"Utah St","Packers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,26,89,"Javon","Hargrave","DT",73,309,"South Carolina St","Steelers");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,27,90,"C.J.","Prosise","RB",72,220,"Notre Dame","Seahawks");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,28,91,"Jacoby","Brissett","QB",76,231,"N.C. State","Patriots");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,29,92,"Brandon","Williams","CB",71,197,"Texas A&M","Cardinals");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,30,93,"Cody","Kessler","QB",73,220,"USC","Browns");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,31,94,"Nick","Vannett","TE",78,257,"Ohio St","Seahawks");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,32,95,"Graham","Glasgow","OL",78,307,"Michigan","Lions");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,33,96,"Vincent","Valentine","DT",76,329,"Nebraska","Patriots");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,34,97,"Rees","Odhiambo","OL",76,314,"Boise St","Seahawks");
INSERT INTO nfl_draft (round, pick_number, overall_pick, first_name, last_name, position, height, weight, college, team)
	VALUES (3,35,98,"Justin","Simmons","S",74,202,"Boston College","Broncos");
